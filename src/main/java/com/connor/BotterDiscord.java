package com.connor;

import net.dv8tion.jda.JDA;
import net.dv8tion.jda.JDABuilder;
import net.dv8tion.jda.utils.SimpleLog;

import javax.security.auth.login.LoginException;

/**
 * Created by Connor on 28/08/2016.
 */
public class BotterDiscord {
    public static final SimpleLog LOG = SimpleLog.getLog("com.connor.BotterDiscord");
    public static final long START = System.currentTimeMillis();

    public static void main(String[] args) throws LoginException, InterruptedException {

        JDA jda = new JDABuilder().setBotToken("")
                .setBulkDeleteSplittingEnabled(false)
                .setAudioEnabled(true)
                .addListener(new Log())
                .addListener(new WhoIsCommand())
                .addListener(new Audio())
                .addListener(new Statscommand())
                .buildAsync();
    }
}
