package com.connor;

import net.dv8tion.jda.MessageBuilder;
import net.dv8tion.jda.entities.User;
import net.dv8tion.jda.events.message.MessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Connor on 28/08/2016.
 */
public class WhoIsCommand extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if (!event.isPrivate()) {
            if (event.getGuild().getId().equals("125227483518861312")) {
                if (event.getTextChannel().getId().equals("169484473333710848")) {
                    if (event.getMessage().getContent().equals("!=Whois")) {
                        User user = event.getAuthor();
                        List<User> joins = new ArrayList<>(event.getGuild().getUsers());
                        Collections.sort(joins, (User a, User b) -> event.getGuild().getJoinDateForUser(a).compareTo(event.getGuild().getJoinDateForUser(b)));
                        int index = joins.indexOf(user);
                        StringBuilder build = new StringBuilder();
                        build.append(joins.get(index - 1).getUsername());
                        build.append(", " + joins.get(index).getUsername());
                        build.append(", " + joins.get(index + 1).getUsername());
                        MessageBuilder builder = new MessageBuilder();
                        builder.appendString("**__USER__**");
                        builder.appendString("\n**Username** : " + event.getAuthor().getUsername());
                        builder.appendString("\n**Discrim** : " + event.getAuthor().getDiscriminator());
                        builder.appendString("\n**ID** : " + event.getAuthor().getId());
                        builder.appendString("\n**Avatar** : " + event.getAuthor().getAvatarUrl());
                        builder.appendString("\n**Join Order** : " + build.toString());
                        builder.appendString("\n\n\n\n**__GUILD__**");
                        builder.appendString("\n**Name** : " + event.getGuild().getName());
                        builder.appendString("\n**Owner** : " + event.getGuild().getOwner());
                        builder.appendString("\n**Users** : " + event.getGuild().getUsers().size());
                        builder.appendString("\n**Roles** : " + event.getGuild().getRoles());
                        builder.appendString("\n\n\n\n**Join Date : **" + event.getGuild().getJoinDateForUser(event.getAuthor()));
                        builder.appendString("\n**Roles** : " + event.getGuild().getRolesForUser(event.getAuthor()));
                        event.getTextChannel().sendMessageAsync(builder.build(), null);

                    }
                }
            }
        }
    }
}
