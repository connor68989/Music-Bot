package com.connor;

import net.dv8tion.jda.events.message.MessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;

import static com.connor.BotterDiscord.LOG;

/**
 * Created by Connor on 28/08/2016.
 */
public class Log extends ListenerAdapter {
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.isPrivate()) {
            StringBuilder builder = new StringBuilder();
            builder.append(event.getAuthor().getUsername() + " " + event.getMessage().getContent());
            LOG.info(builder.toString());
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(event.getGuild().getName() + ":" + event.getTextChannel().getName() + ": " + event.getAuthor().getUsername() + ": " + event.getMessage().getContent());
            LOG.info(builder.toString());
        }
    }
}

