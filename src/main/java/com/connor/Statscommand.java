package com.connor;

import com.connor.util.TimeUtil;
import net.dv8tion.jda.JDA;
import net.dv8tion.jda.audio.AudioSendHandler;
import net.dv8tion.jda.entities.*;
import net.dv8tion.jda.entities.impl.JDAImpl;
import net.dv8tion.jda.events.Event;
import net.dv8tion.jda.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;
import net.dv8tion.jda.managers.AudioManager;
import net.dv8tion.jda.player.MusicPlayer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by Connor on 29/08/2016.
 */
public class Statscommand extends ListenerAdapter {
    public static long getMemoryUsage() {
        Runtime r = Runtime.getRuntime();
        return (r.totalMemory() - r.freeMemory()) / (1024 * 1024);
    }

    public static int getQueueSize(List<VoiceChannel> channels) {
        int[] amount = new int[]{0};
        channels.parallelStream().forEach(c ->
        {
            AudioSendHandler handler = c.getGuild().getAudioManager().getSendingHandler();
            if (!(handler instanceof MusicPlayer)) return;
            amount[0] += ((MusicPlayer) handler).getAudioQueue().size();
        });
        return amount[0];
    }

    public static List<AudioManager> getVoiceConnections(Event shard) {
        List<AudioManager> list = new ArrayList<>();
        list.addAll(((JDAImpl) shard.getJDA()).getAudioManagersMap().values().parallelStream()
                .filter(AudioManager::isConnected).collect(Collectors.toList()));
        return list;
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        if (event.getMessage().getContent().equals("!=Stats")) {
            AtomicReference<Consumer<Message>> consumer = new AtomicReference<>();
            AtomicInteger count = new AtomicInteger();
            consumer.set(message ->
            {
                if (message == null || count.getAndAdd(1) >= 10) return;
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    return;
                }
                message.updateMessageAsync(getStats(event), consumer.get());
            });
            event.getChannel().sendMessageAsync(getStats(event), consumer.get());
        }
    }

    public String getStats(Event event) {
        String uptime = TimeUtil.uptime();
        List<VoiceChannel> channels = getVoiceChannels(event.getJDA());
        int voiceConnections = getVoiceConnections(event).size();
        //int shards = bot.getShards().size();
        int threads = Thread.activeCount();
        int guilds = getGuilds(event).size();
        int text = getTextChannels(event).size();
        int voice = channels.size();
        int queueSize = getQueueSize(channels);
        int pms = getPrivateChannels(event.getJDA()).size();
        long mem = getMemoryUsage();
        return "\n\n**" + guilds + "** Guilds with **" + (text + voice) + "** channels. (**" + text + "** TC, **" + voice + "** VC)\n**"
                + pms + "** Private Channels and **" + voiceConnections + "** Voice connections. (**" + queueSize + "** queue entries)" +
                "\nMemory in MB: [" + "`" + mem + "`/`" + Runtime.getRuntime().totalMemory() / (1024 * 1024) + "`/`" + Runtime.getRuntime().maxMemory() / (1024 * 1024) + "`] (**" + threads + "** Threads)\n" +
                uptime + " Uptime";

    }

    public List<Guild> getGuilds(Event event) {
        List<Guild> guilds = new LinkedList<>();
        guilds.addAll(event.getJDA().getGuilds());
        return guilds;
    }

    public List<TextChannel> getTextChannels(Event event) {
        List<TextChannel> channels = new LinkedList<>();
        channels.addAll(event.getJDA().getTextChannels());
        return channels;
    }

    public List<PrivateChannel> getPrivateChannels(JDA api) {
        List<PrivateChannel> channels = new LinkedList<>();
        channels.addAll(api.getPrivateChannels());
        return channels;
    }

    public List<VoiceChannel> getVoiceChannels(JDA api) {
        List<VoiceChannel> channels = new LinkedList<>();
        channels.addAll(api.getVoiceChannels());
        return channels;
    }
//            --Bot Stats--
//            Name: ErisBot (ID: 189702078958927872)
//            Uptime: 15 hours 10 minutes 57 seconds
//            Threads: 2014
//            Version (BOT/JDA): 1608252/2.2.1_358
//            CPU: 11.3%
//            RAM (USAGE/MAX): 337MB/3641MB
//            Shards: 4
//
//                    --General--
//            Guilds: 3991
//            Users: 117813
//            Text Channels: 18915
//            Voice Channels: 23823
//            Twitter Accounts Tracked: 433
//
//                    --Music--
//            Connections: 73
//            Queue Size: 357
}


