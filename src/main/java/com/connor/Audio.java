package com.connor;

import net.dv8tion.jda.MessageBuilder;
import net.dv8tion.jda.entities.VoiceChannel;
import net.dv8tion.jda.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.hooks.ListenerAdapter;
import net.dv8tion.jda.managers.AudioManager;
import net.dv8tion.jda.player.MusicPlayer;
import net.dv8tion.jda.player.Playlist;
import net.dv8tion.jda.player.source.AudioInfo;
import net.dv8tion.jda.player.source.AudioSource;
import net.dv8tion.jda.player.source.AudioTimestamp;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static net.dv8tion.jda.player.Bot.DEFAULT_VOLUME;

/**
 * Created by Connor on 28/08/2016.
 */

public class Audio extends ListenerAdapter {
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        //If the person who sent the message isn't a known auth'd user, ignore.

        String message = event.getMessage().getContent();
        AudioManager manager = event.getGuild().getAudioManager();
        MusicPlayer player;
        if (manager.getSendingHandler() == null) {
            player = new MusicPlayer();
            player.setVolume(DEFAULT_VOLUME);
            manager.setSendingHandler(player);
        } else {
            player = (MusicPlayer) manager.getSendingHandler();
        }

        if (message.startsWith("volume ")) {
            float volume = Float.parseFloat(message.substring("volume ".length()));
            volume = Math.min(1F, Math.max(0F, volume));
            player.setVolume(volume);
            event.getChannel().sendMessageAsync("Volume was changed to: " + volume, null);
        }
        if (message.equals("volume")) {
            event.getChannel().sendMessageAsync(String.valueOf(player.getVolume()), null);
        }
        if (message.equals("list")) {
            List<AudioSource> queue = player.getAudioQueue();
            if (queue.isEmpty()) {
                event.getChannel().sendMessageAsync("The queue is currently empty!", null);
                return;
            }


            MessageBuilder builder = new MessageBuilder();
            builder.appendString("__Current Queue.  Entries: " + queue.size() + "__\n");
            for (int i = 0; i < queue.size() && i < 10; i++) {
                AudioInfo info = queue.get(i).getInfo();
//                builder.appendString("**(" + (i + 1) + ")** ");
                if (info == null)
                    builder.appendString("*Could not get info for this song.*");
                else {
                    AudioTimestamp duration = info.getDuration();
                    builder.appendString("`[");
                    if (duration == null)
                        builder.appendString("N/A");
                    else
                        builder.appendString(duration.getTimestamp());
                    builder.appendString("]` " + info.getTitle() + "\n");
                }
            }

            boolean error = false;
            int totalSeconds = 0;
            for (AudioSource source : queue) {
                AudioInfo info = source.getInfo();
                if (info == null || info.getDuration() == null) {
                    error = true;
                    continue;
                }
                totalSeconds += info.getDuration().getTotalSeconds();
            }

            builder.appendString("\nTotal Queue Time Length: " + AudioTimestamp.fromSeconds(totalSeconds).getTimestamp());
            if (error)
                builder.appendString("`An error occured calculating total time. Might not be completely valid.");
            event.getChannel().sendMessageAsync(builder.build(), null);
        }
        if (message.equals("nowplaying")) {
            if (player.isPlaying()) {
                AudioTimestamp currentTime = player.getCurrentTimestamp();
                AudioInfo info = player.getCurrentAudioSource().getInfo();
                if (info.getError() == null) {
                    event.getChannel().sendMessageAsync(
                            "**Playing:** " + info.getTitle() + "\n" +
                                    "**Time:**    [" + currentTime.getTimestamp() + " / " + info.getDuration().getTimestamp() + "]", null);
                } else {
                    event.getChannel().sendMessageAsync(
                            "**Playing:** Info Error. Known source: " + player.getCurrentAudioSource().getSource() + "\n" +
                                    "**Time:**    [" + currentTime.getTimestamp() + " / (N/A)]", null);
                }
            } else {
                event.getChannel().sendMessageAsync("The player is not currently playing anything!", null);
            }
        }

        //Start an audio connection with a VoiceChannel
        if (message.startsWith("join ")) {
            //Separates the name of the channel so that we can search for it
            String chanName = message.substring(5);

            //Scans through the VoiceChannels in this Guild, looking for one with a case-insensitive matching name.
            VoiceChannel channel = event.getGuild().getVoiceChannels().stream().filter(
                    vChan -> vChan.getName().equalsIgnoreCase(chanName))
                    .findFirst().orElse(null);  //If there isn't a matching name, return null.
            if (channel == null) {
                event.getChannel().sendMessageAsync("There isn't a VoiceChannel in this Guild with the name: '" + chanName + "'", null);
                return;
            }
            manager.openAudioConnection(channel);
        }
        //Disconnect the audio connection with the VoiceChannel.
        if (message.equals("leave"))
            manager.closeAudioConnection();

        if (message.equals("skip")) {
            player.skipToNext();
            event.getChannel().sendMessageAsync("Skipped the current song.", null);
        }

        if (message.equals("repeat")) {
            if (player.isRepeat()) {
                player.setRepeat(false);
                event.getChannel().sendMessageAsync("The player has been set to **not** repeat.", null);
            } else {
                player.setRepeat(true);
                event.getChannel().sendMessageAsync("The player been set to repeat.", null);
            }
        }

        if (message.equals("shuffle")) {
            if (player.isShuffle()) {
                player.setShuffle(false);
                event.getChannel().sendMessageAsync("The player has been set to **not** shuffle.", null);
            } else {
                player.setShuffle(true);
                event.getChannel().sendMessageAsync("The player been set to shuffle.", null);
            }
        }

        if (message.equals("reset")) {
            player.stop();
            player = new MusicPlayer();
            player.setVolume(DEFAULT_VOLUME);
            manager.setSendingHandler(player);
            event.getChannel().sendMessageAsync("Music player has been completely reset.", null);
        }

        //Start playing audio with our FilePlayer. If we haven't created and registered a FilePlayer yet, do that.
        if (message.startsWith("play")) {
            //If no URL was provided.
            if (message.equals("play")) {
                if (player.isPlaying()) {
                    event.getChannel().sendMessageAsync("Player is already playing!", null);
                    return;
                } else if (player.isPaused()) {
                    player.play();
                    event.getChannel().sendMessageAsync("Playback as been resumed.", null);
                } else {
                    if (player.getAudioQueue().isEmpty())
                        event.getChannel().sendMessageAsync("The current audio queue is empty! Add something to the queue first!", null);
                    else {
                        player.play();
                        event.getChannel().sendMessageAsync("Player has started playing!", null);
                    }
                }
            } else if (message.startsWith("play ")) {
                String msg = "";
                String url = message.substring("play ".length());
                Playlist playlist = Playlist.getPlaylist(url);
                List<AudioSource> sources = new LinkedList(playlist.getSources());
//                AudioSource source = new RemoteSource(url);
//                AudioSource source = new LocalSource(new File(url));
//                AudioInfo info = source.getInfo();   //Preload the audio info.
                if (sources.size() > 1) {
                    event.getChannel().sendMessageAsync("Found a playlist with **" + sources.size() + "** entries.\n" +
                            "Proceeding to gather information and queue sources. This may take some time...", null);
                    final MusicPlayer fPlayer = player;
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            for (Iterator<AudioSource> it = sources.iterator(); it.hasNext(); ) {
                                AudioSource source = it.next();
                                AudioInfo info = source.getInfo();
                                List<AudioSource> queue = fPlayer.getAudioQueue();
                                if (info.getError() == null) {
                                    queue.add(source);
                                    if (fPlayer.isStopped())
                                        fPlayer.play();
                                } else {
                                    event.getChannel().sendMessageAsync("Error detected, skipping source. Error:\n" + info.getError(), null);
                                    it.remove();
                                }
                            }
                            event.getChannel().sendMessageAsync("Finished queuing provided playlist. Successfully queued **" + sources.size() + "** sources", null);
                        }
                    };
                    thread.start();
                } else {
                    AudioSource source = sources.get(0);
                    AudioInfo info = source.getInfo();
                    if (info.getError() == null) {
                        player.getAudioQueue().add(source);
                        msg += "The provided URL has been added the to queue";
                        if (player.isStopped()) {
                            player.play();
                            msg += " and the player has started playing";
                        }
                        event.getChannel().sendMessageAsync(msg + ".", null);
                    } else {
                        event.getChannel().sendMessageAsync("There was an error while loading the provided URL.\n" +
                                "Error: " + info.getError(), null);
                    }
                }
            }
        }
        if (message.equals("pause")) {
            player.pause();
            event.getChannel().sendMessageAsync("Playback has been paused.", null);
        }
        if (message.equals("stop")) {
            player.stop();
            event.getChannel().sendMessageAsync("Playback has been completely stopped.", null);
        }
        if (message.equals("restart")) {
            if (player.isStopped()) {
                if (player.getPreviousAudioSource() != null) {
                    player.reload(true);
                    event.getChannel().sendMessageAsync("The previous song has been restarted.", null);
                } else {
                    event.getChannel().sendMessageAsync("The player has never played a song, so it cannot restart a song.", null);
                }
            } else {
                player.reload(true);
                event.getChannel().sendMessageAsync("The currently playing song has been restarted!", null);
            }
        }
    }
}

